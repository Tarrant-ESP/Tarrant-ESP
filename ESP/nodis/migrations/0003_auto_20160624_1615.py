# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('nodis', '0002_auto_20160513_1020'),
    ]

    operations = [
        migrations.AlterField(
            model_name='caseactivehistory',
            name='change_reason',
            field=models.CharField(max_length=2, choices=[(b'Q', b'Q - Qualifying events'), (b'D', b'D - Disqualifying events'), (b'E', b'E - Elapsed time')]),
        ),
        migrations.AlterField(
            model_name='caseactivehistory',
            name='content_type',
            field=models.ForeignKey(blank=True, to='contenttypes.ContentType', null=True),
        ),
        migrations.AlterField(
            model_name='caseactivehistory',
            name='object_id',
            field=models.PositiveIntegerField(db_index=True, null=True, blank=True),
        ),
    ]
