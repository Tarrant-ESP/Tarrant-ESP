# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('conf', '0002_load_initial_data'),
    ]

    operations = [
        migrations.AlterField(
            model_name='labtestmap',
            name='donotsend_results',
            field=models.ManyToManyField(related_name='do_not_send', to='conf.ResultString', blank=True),
        ),
        migrations.AlterField(
            model_name='labtestmap',
            name='excluded_indeterminate_strings',
            field=models.ManyToManyField(related_name='excluded_indeterminate_set', to='conf.ResultString', blank=True),
        ),
        migrations.AlterField(
            model_name='labtestmap',
            name='excluded_negative_strings',
            field=models.ManyToManyField(related_name='excluded_negative_set', to='conf.ResultString', blank=True),
        ),
        migrations.AlterField(
            model_name='labtestmap',
            name='excluded_positive_strings',
            field=models.ManyToManyField(related_name='excluded_positive_set', to='conf.ResultString', blank=True),
        ),
        migrations.AlterField(
            model_name='labtestmap',
            name='extra_indeterminate_strings',
            field=models.ManyToManyField(related_name='extra_indeterminate_set', to='conf.ResultString', blank=True),
        ),
        migrations.AlterField(
            model_name='labtestmap',
            name='extra_negative_strings',
            field=models.ManyToManyField(related_name='extra_negative_set', to='conf.ResultString', blank=True),
        ),
        migrations.AlterField(
            model_name='labtestmap',
            name='extra_positive_strings',
            field=models.ManyToManyField(related_name='extra_positive_set', to='conf.ResultString', blank=True),
        ),
    ]
