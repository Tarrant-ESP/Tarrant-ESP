#!/usr/bin/env python
'''
                                  ESP Health
                         Notifiable Diseases Framework
Pluggable Report Generator


@author: Jason McVetta <jason.mcvetta@gmail.com>
@organization: Channing Laboratory http://www.channing.harvard.edu
@copyright: (c) 2009 Channing Laboratory
@license: LGPL
'''

from optparse import make_option

from django.core.management.base import BaseCommand

from ESP.nodis.base import Report


class Command(BaseCommand):
    
    help = 'Run a report'
    
    args = 'NAME'

    def add_arguments(self, parser):
        parser.add_argument('NAME', nargs=1, type=str)
        parser.add_argument('--list', action='store_true', dest='list', help='List available reports'),

    def handle(self, *args, **options):
        if options['list']:
            for rep in Report.get_all():
                print rep.short_name
            return
        report_name = options['NAME']
        all_reports = {}
        for rep in Report.get_all():
            all_reports[rep.short_name] = rep
        assert report_name in all_reports # Is this a valid report name?
        report_object = all_reports[report_name]   
        report_object.run()