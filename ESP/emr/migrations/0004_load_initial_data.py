# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.core.management import call_command


def load_fixture(apps, schema_editor):
    call_command('loaddata', 'data', app_label='emr')

class Migration(migrations.Migration):

    dependencies = [
        ('emr', '0003_auto_20160513_1020'),
    ]

    operations = [
        migrations.RunPython(load_fixture),
    ]
