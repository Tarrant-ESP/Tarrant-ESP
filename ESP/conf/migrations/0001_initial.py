# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('static', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ConditionConfig',
            fields=[
                ('name', models.CharField(max_length=255, serialize=False, verbose_name=b'Condition Name', primary_key=True)),
                ('initial_status', models.CharField(default=b'AR', max_length=8, choices=[(b'AR', b'AR - Awaiting Review'), (b'UR', b'UR - Under Review'), (b'RM', b'RM - Review by MD'), (b'FP', b'FP - False Positive - Do NOT Process'), (b'Q', b'Q - Confirmed Case, Transmit to Health Department'), (b'S', b'S - Transmitted to Health Department'), (b'NO', b'NO - Do NOT send cases'), (b'RQ', b'RQ - Re-queued for transmission. Updated after prior transmission'), (b'RS', b'RS - Re-sent after update subsequent to prior transmission')])),
                ('lab_days_before', models.IntegerField(default=28)),
                ('lab_days_after', models.IntegerField(default=28)),
                ('dx_code_days_before', models.IntegerField(default=28)),
                ('dx_code_days_after', models.IntegerField(default=28)),
                ('med_days_before', models.IntegerField(default=28)),
                ('med_days_after', models.IntegerField(default=28)),
                ('ext_var_days_before', models.IntegerField(default=28)),
                ('ext_var_days_after', models.IntegerField(default=28)),
                ('reinfection_days', models.IntegerField(default=28)),
                ('url_name', models.CharField(max_length=100, null=True, verbose_name=b'Optional url name for case details', blank=True)),
            ],
            options={
                'verbose_name': 'Condition Configuration',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Extended_VariablesMap',
            fields=[
                ('native_string', models.CharField(max_length=255)),
                ('abstract_ext_var', models.CharField(max_length=255, serialize=False, primary_key=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='HL7Map',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('model', models.CharField(max_length=100)),
                ('variable', models.CharField(max_length=100)),
                ('value', models.CharField(max_length=100)),
                ('hl7', models.ForeignKey(to='static.hl7_vocab', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='IgnoredCode',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('native_code', models.CharField(unique=True, max_length=100)),
            ],
            options={
                'verbose_name': 'Ignored Test Code',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ImmuExclusion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('non_immu_name', models.CharField(unique=True, max_length=200)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='LabTestMap',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('test_name', models.SlugField(verbose_name=b'Name of Abstract Lab Test')),
                ('native_code', models.CharField(help_text=b'Native test code from source EMR system', max_length=100, verbose_name=b'Test Code', db_index=True)),
                ('code_match_type', models.CharField(default=b'exact', help_text=b'Match type for test code', max_length=32, choices=[(b'exact', b'Exact Match (case sensitive)'), (b'iexact', b'Exact Match (NOT case sensitive)'), (b'startswith', b'Starts With (case sensitive)'), (b'istartswith', b'Starts With (NOT case sensitive)'), (b'endswith', b'Ends With (case sensitive)'), (b'iendswith', b'Ends With (NOT case sensitive)'), (b'contains', b'Contains (case sensitive)'), (b'icontains', b'Contains (NOT case sensitive)')])),
                ('record_type', models.CharField(default=b'both', help_text=b'Does this map relate to lab orders, results, or both?', max_length=8, choices=[(b'order', b'Lab Test Orders'), (b'result', b'Lab Test Results'), (b'both', b'Both Lab Test Orders and Results')])),
                ('threshold', models.FloatField(help_text=b'Fallback positive threshold for tests without reference high', null=True, blank=True)),
                ('reportable', models.BooleanField(default=True, db_index=True, verbose_name=b'Is test reportable?')),
                ('output_code', models.CharField(max_length=100, null=True, verbose_name=b'Test code for template output', blank=True)),
                ('output_name', models.CharField(max_length=255, null=True, verbose_name=b'Test name for template output', blank=True)),
                ('snomed_pos', models.CharField(max_length=255, null=True, verbose_name=b'SNOMED positive code', blank=True)),
                ('snomed_neg', models.CharField(max_length=255, null=True, verbose_name=b'SNOMED neg code', blank=True)),
                ('snomed_ind', models.CharField(max_length=255, null=True, verbose_name=b'SNOMED indeterminate code', blank=True)),
                ('reinf_output_code', models.CharField(max_length=255, null=True, verbose_name=b'LOINC re-infection code', blank=True)),
                ('notes', models.TextField(null=True, blank=True)),
            ],
            options={
                'verbose_name': 'Lab Test Map',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ReportableDx_Code',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('notes', models.TextField(null=True, blank=True)),
                ('condition', models.ForeignKey(to='conf.ConditionConfig')),
                ('dx_code', models.ForeignKey(to='static.Dx_code')),
            ],
            options={
                'verbose_name': 'Reportable dx Code',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ReportableExtended_Variables',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('notes', models.TextField(null=True, blank=True)),
                ('abstract_ext_var', models.ForeignKey(to='conf.Extended_VariablesMap')),
                ('condition', models.ForeignKey(to='conf.ConditionConfig')),
            ],
            options={
                'verbose_name': 'Reportable Extended Variables',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ReportableLab',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('native_name', models.CharField(max_length=255, verbose_name=b'Abstract Lab Name', db_index=True)),
                ('notes', models.TextField(null=True, blank=True)),
                ('condition', models.ForeignKey(to='conf.ConditionConfig')),
            ],
            options={
                'verbose_name': 'Reportable Lab Test',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ReportableMedication',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('drug_name', models.CharField(max_length=255)),
                ('notes', models.TextField(null=True, blank=True)),
                ('condition', models.ForeignKey(to='conf.ConditionConfig')),
            ],
            options={
                'verbose_name': 'Reportable Medication',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ResultString',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.CharField(max_length=128)),
                ('indicates', models.CharField(max_length=8, choices=[(b'pos', b'Positive'), (b'neg', b'Negative'), (b'ind', b'Indeterminate')])),
                ('match_type', models.CharField(default=b'istartswith', help_text=b'Match type for string', max_length=32, choices=[(b'exact', b'Exact Match (case sensitive)'), (b'iexact', b'Exact Match (NOT case sensitive)'), (b'startswith', b'Starts With (case sensitive)'), (b'istartswith', b'Starts With (NOT case sensitive)'), (b'endswith', b'Ends With (case sensitive)'), (b'iendswith', b'Ends With (NOT case sensitive)'), (b'contains', b'Contains (case sensitive)'), (b'icontains', b'Contains (NOT case sensitive)')])),
                ('applies_to_all', models.BooleanField(default=False, help_text=b'Match this string for ALL tests.  If not checked, string must be explicitly specified in Lab Test Map')),
            ],
            options={
                'ordering': ['value'],
                'verbose_name': 'Result String',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SiteHL7',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('location', models.CharField(max_length=40)),
                ('element', models.CharField(max_length=80)),
                ('value', models.CharField(max_length=255, null=True)),
            ],
            options={
                'verbose_name': 'HL7 site date',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='VaccineCodeMap',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('native_code', models.CharField(max_length=128)),
                ('native_name', models.CharField(max_length=200)),
                ('canonical_code', models.ForeignKey(to='static.Vaccine', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='VaccineManufacturerMap',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('native_name', models.CharField(unique=True, max_length=200)),
                ('canonical_code', models.ForeignKey(to='static.ImmunizationManufacturer', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='sitehl7',
            unique_together=set([('location', 'element')]),
        ),
        migrations.AlterUniqueTogether(
            name='reportablemedication',
            unique_together=set([('drug_name', 'condition')]),
        ),
        migrations.AlterUniqueTogether(
            name='reportablelab',
            unique_together=set([('native_name', 'condition')]),
        ),
        migrations.AlterUniqueTogether(
            name='reportableextended_variables',
            unique_together=set([('abstract_ext_var', 'condition')]),
        ),
        migrations.AlterUniqueTogether(
            name='reportabledx_code',
            unique_together=set([('dx_code', 'condition')]),
        ),
        migrations.AddField(
            model_name='labtestmap',
            name='donotsend_results',
            field=models.ManyToManyField(related_name=b'do_not_send', null=True, to='conf.ResultString', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='labtestmap',
            name='excluded_indeterminate_strings',
            field=models.ManyToManyField(related_name=b'excluded_indeterminate_set', null=True, to='conf.ResultString', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='labtestmap',
            name='excluded_negative_strings',
            field=models.ManyToManyField(related_name=b'excluded_negative_set', null=True, to='conf.ResultString', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='labtestmap',
            name='excluded_positive_strings',
            field=models.ManyToManyField(related_name=b'excluded_positive_set', null=True, to='conf.ResultString', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='labtestmap',
            name='extra_indeterminate_strings',
            field=models.ManyToManyField(related_name=b'extra_indeterminate_set', null=True, to='conf.ResultString', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='labtestmap',
            name='extra_negative_strings',
            field=models.ManyToManyField(related_name=b'extra_negative_set', null=True, to='conf.ResultString', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='labtestmap',
            name='extra_positive_strings',
            field=models.ManyToManyField(related_name=b'extra_positive_set', null=True, to='conf.ResultString', blank=True),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='labtestmap',
            unique_together=set([('test_name', 'native_code', 'code_match_type', 'record_type')]),
        ),
        migrations.AlterUniqueTogether(
            name='extended_variablesmap',
            unique_together=set([('abstract_ext_var', 'native_string')]),
        ),
    ]
