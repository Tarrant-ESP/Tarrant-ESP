import unittest
from .utils import float_or_none

class UtilTestCase(unittest.TestCase):

    def test_float_or_none(self):
        self.assertEqual(float_or_none('123'), 123, 'Integer should match')
        self.assertEqual(float_or_none('123.123'), 123.123, 'Normal float should match')
        self.assertIsNone(float_or_none('infinity'), 'Infinity returns None on purpose')
        self.assertEqual(float_or_none('123,123'), 123123, 'Comma delimiters should result in floats')
        self.assertIsNone(float_or_none('123A123'), 'Alpha characters should result in None')